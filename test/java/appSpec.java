package app;

import app.model.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.CoreMatchers.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonParseException;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@DisplayName("Usability unit tests")
@WebMvcTest
public class appSpec {

    @Autowired
    private MockMvc mockMvc;

    private final String VALID_VIN = "12345678901234567";  // TODO: make this better
    private final String VALID_MODEL = "model_todo";  // TODO: make this better


    @Test
    public void should_load_car_from_file()
            throws JsonParseException, IOException, Exception {

        String encryptedCarString = Files.readAllLines(Paths.get("encryptedCarString.txt")).get(0);
        System.out.println("XXXXXXXXXXXXXX encryptedCarString = " + encryptedCarString);

        CarLoader loader = new CarLoader();
        Car decryptedCar = loader.decryptCar(encryptedCarString);
        System.out.println("XXXXXXXXXXXXXX decryptedCar = " + decryptedCar);


    }

    @Test
    public void temp_utility_enrypt_car()
            throws JsonParseException, IOException, Exception {

        String dercytedValue = "1|12345678901234567|myModel";
        byte[] key = "secret7890123456".getBytes("UTF-8");  // NOTE: AES requires 16 byte key.
        String encryptedCarString = CarLoader.encrypt(dercytedValue, key);
        System.out.println("XXXXXXXXXXXXXX encryptedCarString = " + encryptedCarString);
        FileWriter myWriter = new FileWriter("encryptedCarString.txt");
        myWriter.write(encryptedCarString);
        myWriter.close();

    }

    @Test
    public void should_accept_validate_car()
            throws JsonParseException, IOException, Exception {

        Car car = new Car(new CarId(1), new Vin(VALID_VIN), new Model(VALID_MODEL));

    }

    @Test
    public void should_reject_null_vin()
            throws JsonParseException, IOException, Exception {

        try {
            Car car = new Car(new CarId(1), null, new Model(VALID_MODEL));
            throw new Exception("Expected constructor above to fail.");
        } catch (CarException ex) {
        }
    }

    @Test
    public void should_reject_empty_vin()
            throws JsonParseException, IOException, Exception {

        try {
            Car car = new Car(new CarId(1), new Vin("    "), new Model(VALID_MODEL));
            throw new Exception("Expected constructor above to fail.");
        } catch (CarException ex) {
        }

    }

    @Test
    public void should_reject_invalidate_vin_1()
            throws JsonParseException, IOException, Exception {

        try {
            Car car = new Car(new CarId(1), new Vin("invalid_vin"), new Model(VALID_MODEL));
            throw new Exception("Expected constructor above to fail.");
        } catch (CarException ex) {
        }

    }

    @Test
    public void should_reject_invalidate_vin_2()
            throws JsonParseException, IOException, Exception {

        try {
            Car car = new Car(new CarId(1), new Vin("123456789$1234567"), new Model(VALID_MODEL));
            throw new Exception("Expected constructor above to fail.");
        } catch (CarException ex) {
        }

    }

    @Test
    public void should_reject_null_model()
            throws JsonParseException, IOException, Exception {

        try {
            Car car = new Car(new CarId(1), new Vin(VALID_VIN), new Model(null));
            throw new Exception("Expected constructor above to fail.");
        } catch (CarException ex) {
        }

    }

    @Test
    public void should_reject_empty_model()
            throws JsonParseException, IOException, Exception {

        try {
            Car car = new Car(new CarId(1), new Vin(VALID_VIN), new Model("     "));
            throw new Exception("Expected constructor above to fail.");
        } catch (CarException ex) {
        }

    }

}
