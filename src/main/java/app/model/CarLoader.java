package app.model;

import app.Car;
import org.apache.tomcat.util.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Arrays;

public class CarLoader {

    public Car decryptCar(String encryptedBlob) throws Exception {
        byte[] keyBytes = System.getenv("SECRET_KEY").getBytes("UTF-8");  // TODO: handle null env var
        String decryptedValue = decrypt(encryptedBlob, keyBytes);
        Arrays.fill(keyBytes, (byte)0);

        System.out.println("XXXXXXXXX decryptedValue: " + decryptedValue);
        String[] carSplit = decryptedValue.split("[|]");
        System.out.println("XXXXXXXXX carId: " + carSplit[0]);
        System.out.println("XXXXXXXXX vin: " + carSplit[1]);
        System.out.println("XXXXXXXXX model: " + carSplit[2]);

        CarId carId = new CarId(Integer.parseInt(carSplit[0]));
        Vin vin = new Vin(carSplit[1]);
        Model model = new Model(carSplit[2]);

        return new Car(carId, vin, model);
    }

    private static final String initVector = "encryptionIntVec";

    public static String encrypt(String value, byte[] key) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());
            return Base64.encodeBase64String(encrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private static String decrypt(String encrypted, byte[] keyBytes) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(keyBytes, "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));

            return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

}
