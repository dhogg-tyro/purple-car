package app.model;

import java.util.regex.Pattern;

public class Vin {

    private final String vin;

    public Vin(String vin) throws CarException {
        validateVin(vin);
        this.vin = vin;
    }

    public String getvin() {
        return this.vin;
    }

    private void validateVin(String input) throws CarException {
        // https://www.autocheck.com/vehiclehistory/vin-basics

        if (input == null) {
            throw new CarException("vin is null");
        }

        if (input.trim().length() == 0) {
            throw new CarException("vin is empty");
        }

        if (input.length() != 17) {
            throw new CarException("vin length is not 17");
        }

        if (!Pattern.matches("^[A-Z0-9]+$", input)) {
            throw new CarException("vin syntax failure");
        }

        // TODO: more vin checks
    }
}
