package app.model;

public class CarId {

    private final Integer carId;

    public CarId(Integer carId) throws CarException {
        validateCarId(carId);
        this.carId = carId;
    }

    public Integer getvin() {
        return this.carId;
    }

    private void validateCarId(Integer carId) throws CarException {
        if (carId < 1) {
            throw new CarException("carId is less than 1");
        }
    }
}
