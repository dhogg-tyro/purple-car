package app.model;

public class Model {

    private final String model;

    public Model(String model) throws CarException {
        validateModel(model);
        this.model = model;
    }

    public String getModel() {
        return this.model;
    }

    private void validateModel(String input) throws CarException {
        if (input == null) {
            throw new CarException("model is null");
        }

        if (input.trim().length() == 0) {
            throw new CarException("model is empty");
        }
    }
}
