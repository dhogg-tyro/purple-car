package app;

import app.model.CarException;
import app.model.Vin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringEscapeUtils;


@RestController
public class CarSaleController {

    @RequestMapping("/")
    public String render() {
        String escaped = StringEscapeUtils.escapeHtml4("Welcome to The Purple Car, an online car sale company");
        return escaped;
    }

    @RequestMapping("/add")
    public String render(@RequestBody Car car) {
        return "Added car: " + car.toString();
    }

}
