package app;

import app.model.*;

public class Car {

    private CarId carID;
    private Vin vin;
    private Model model;
    //private String plateNo;
    //private File photo;

    public Car() {
        super();
    }

    public Car(CarId carID, Vin vin, Model model) throws Exception {
        if (carID == null) {
            throw new CarException("carID cannot be null");
        }

        if (vin == null) {
            throw new CarException("vin cannot be null");
        }

        if (model == null) {
            throw new CarException("model cannot be null");
        }

        this.carID = carID;
        this.vin = vin;
        this.model = model;
    }

    public CarId getcarID() {
        return this.carID;
    }

    public Vin getvin() {
        return this.vin;
    }

    public Model getmodel() {
        return this.model;
    }

    @Override
    public String toString() {
        return this.carID.toString() + " "  + this.vin + " " + this.model;
    }

    public static Car loadCarFromFile(String filename) throws Exception {
        String encryptedCarBlob = "1|12345678901234567|myModel";  // TODO: encrypt and load value from file
        CarLoader loader = new CarLoader();
        Car car = loader.decryptCar(encryptedCarBlob);

        return car;
    }

}
